package com.gamesys.application.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gamesys.application.model.User;
import com.gamesys.application.service.RegistrationService;

@RestController
public class UserController {

	@Autowired
	private RegistrationService userRegistrationService;

	@RequestMapping(method = RequestMethod.POST, path = "/add")
	public ResponseEntity<Object> addUser(@RequestBody User newUser) {
		String error = userRegistrationService.validate(newUser);
		if(error != null) {
			return new ResponseEntity<Object>(error, HttpStatus.BAD_REQUEST);
		}
		userRegistrationService.add(newUser);
		return new ResponseEntity<Object>(newUser, HttpStatus.OK);
	}
}
