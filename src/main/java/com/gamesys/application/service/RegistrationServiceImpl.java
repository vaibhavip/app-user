package com.gamesys.application.service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gamesys.application.model.User;
import com.gamesys.application.repository.UserRepository;

@Component
public class RegistrationServiceImpl implements RegistrationService{

	private static final String PASSWORD_PATTERN = "((?=.*[a-z])(?=.*[A-Z])(?=.*\\d).{4,})";
	
	@Autowired
	ExclusionService exclusionService;
	
	@Autowired
	UserRepository userRepository;
	
	@Override
	public String validate(User user) {
		
		if (user.getUsername() == null || !user.getUsername().matches("[a-zA-Z0-9]+")) {
			return "Username is invalid/not present";
		}
		if (user.getPassword() == null || !validatePassword(user.getPassword())) {
			return "Enter valid password";
		}
		try {
			validateDate(user.getDateOfBirth());
		} catch(DateTimeParseException | IllegalArgumentException e) {
			return "Date of birth is invalid/not present";
		}
		if (user.getSsn() == null || user.getSsn().isEmpty()) {
			return "Enter SSN value to register user";
		}
		if (!(exclusionService.validate(user.getDateOfBirth(), user.getSsn()))) {
			return "Cannot register user as the user is excluded";
		} 
		if (!userRepository.findByUsernameOrSsn(user.getUsername(), user.getSsn()).isEmpty()) {
			return "The user already exists";
		}
		return null;
	}
	
	@Override
	public Long add(User user) {
		userRepository.save(user);
		return user.getId();
	}
	
	private LocalDate validateDate(String date) {
		if(date == null) {
			throw new IllegalArgumentException("Date must be present");
		}
		return LocalDate.parse(date, DateTimeFormatter.ISO_DATE);
	}
	
	private boolean validatePassword(final String password) {
		Pattern pattern = null;
		Matcher matcher;
		pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        return matcher.matches();
 
    }	
}
