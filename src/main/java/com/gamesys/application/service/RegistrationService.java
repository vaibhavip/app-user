package com.gamesys.application.service;

import com.gamesys.application.model.User;

public interface RegistrationService {

	String validate(User user);
	
	Long add(User user);
}
