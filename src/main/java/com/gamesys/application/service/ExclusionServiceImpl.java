package com.gamesys.application.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gamesys.application.model.ExcludedUser;
import com.gamesys.application.repository.ExcludedUserRepository;

@Component
public class ExclusionServiceImpl implements ExclusionService {

	@Autowired
	ExcludedUserRepository excludedUserRepository;
	
	@Override
	public boolean validate(String dateOfBirth, String ssn) {
		ExcludedUser excludedUser = excludedUserRepository.findByDateOfBirthAndSsn(dateOfBirth, ssn);
		return excludedUser == null;
	}
}
