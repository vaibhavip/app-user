package com.gamesys.application.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gamesys.application.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	List<User> findByUsernameOrSsn(String username, String ssn);
}
