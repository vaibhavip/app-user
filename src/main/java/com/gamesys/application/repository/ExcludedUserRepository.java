package com.gamesys.application.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gamesys.application.model.ExcludedUser;

@Repository
public interface ExcludedUserRepository extends JpaRepository<ExcludedUser, Long> {

	ExcludedUser findByDateOfBirthAndSsn(String dateOfBirth, String ssn);
}
