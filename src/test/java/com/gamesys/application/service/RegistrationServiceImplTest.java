package com.gamesys.application.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.gamesys.application.model.User;
import com.gamesys.application.repository.UserRepository;

@RunWith(MockitoJUnitRunner.class)
public class RegistrationServiceImplTest {
	
	@Mock
	private ExclusionService exclusionService;
	
	@Mock
	private UserRepository mockRepository;

	@InjectMocks
	RegistrationServiceImpl userService = new RegistrationServiceImpl();
	
	private static final String VALID_USERNAME = "user123";
    private static final String VALID_PASSWORD = "Pass123";
    private static final String VALID_DATE_OF_BIRTH = "2005-01-01";
    private static final String VALID_SSN = "99999999";
    
    @Test
    public void checkIfUsernameIsNull() {
    	String message = userService.validate(createUser(null, VALID_PASSWORD, VALID_DATE_OF_BIRTH, VALID_SSN));
    	assertEquals("Username is invalid/not present", message);
    }

	@Test
	public void checkIfUsernameIsValid() {
		String message = userService.validate(createUser("abc_123", VALID_PASSWORD, VALID_DATE_OF_BIRTH, VALID_SSN));
		assertEquals("Username is invalid/not present", message);
	}

	@Test
    public void checkIfPasswordIsNull() {
    	String message = userService.validate(createUser(VALID_USERNAME, null, VALID_DATE_OF_BIRTH, VALID_SSN));
    	assertEquals("Enter valid password", message);
    }
	
	@Test
	public void checkIfPasswordIsValid() {
		String message = userService.validate(createUser(VALID_USERNAME, "pass123", VALID_DATE_OF_BIRTH, VALID_SSN));
		assertEquals("Enter valid password", message);
	}

	@Test
    public void checkIfDateOfBirthIsNull() {
    	String message = userService.validate(createUser(VALID_USERNAME, VALID_PASSWORD, null, VALID_SSN));
    	assertEquals("Date of birth is invalid/not present", message);
    }
	
	@Test
	public void checkIfDateOfBirthIsValid() {
		String message = userService.validate(createUser(VALID_USERNAME, VALID_PASSWORD, "2005-1-01", VALID_SSN));
		assertEquals("Date of birth is invalid/not present", message);
	}
	
	@Test
	public void checkIfSsnIsNull() {
		String message = userService.validate(createUser(VALID_USERNAME, VALID_PASSWORD, VALID_DATE_OF_BIRTH, null));
		assertEquals("Enter SSN value to register user", message);
	}

	@Test
	public void checkIfUserAlreadyExists() {
		List<User> users = new ArrayList<>();
		users.add(new User());
		Mockito.when(exclusionService.validate(Mockito.anyString(), Mockito.anyString())).thenReturn(true);
		Mockito.when(mockRepository.findByUsernameOrSsn(Mockito.anyString(), Mockito.anyString())).thenReturn(users);
		String message = userService.validate(createUser(VALID_USERNAME, VALID_PASSWORD, VALID_DATE_OF_BIRTH, "775385075"));
		assertEquals("The user already exists", message);
	}
	
	@Test
	public void whenValidUser_thenCorrectResponse() throws Exception {
		Mockito.when(exclusionService.validate(Mockito.anyString(), Mockito.anyString())).thenReturn(true);
		assertNull(userService.validate(createUser(VALID_USERNAME, VALID_PASSWORD, VALID_DATE_OF_BIRTH, VALID_SSN)));
	}
	
	private User createUser(String username, String password, String dateOfBirth, String ssn) {
		User user = new User();
		user.setUsername(username);
		user.setPassword(password);
		user.setDateOfBirth(dateOfBirth);
		user.setSsn(ssn);
		return user;
    }
}
