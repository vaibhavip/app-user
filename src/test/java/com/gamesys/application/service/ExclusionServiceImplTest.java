package com.gamesys.application.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.gamesys.application.model.User;
import com.gamesys.application.repository.ExcludedUserRepository;

@RunWith(MockitoJUnitRunner.class)
public class ExclusionServiceImplTest {
	
	@Mock
	private ExclusionService exclusionService;
	
	@Mock
	private ExcludedUserRepository mockRepository;
	
	@InjectMocks
	RegistrationServiceImpl userService = new RegistrationServiceImpl();
	
	@Test
	public void checkIfUserIsExcluded() {
		Mockito.when(exclusionService.validate(Mockito.anyString(), Mockito.anyString())).thenReturn(false);
		String message = userService.validate(createUser("user123", "Pass123", "2005-01-01", "99999999"));
		assertEquals("Cannot register user as the user is excluded", message);
	}
	
	private User createUser(String username, String password, String dateOfBirth, String ssn) {
		User user = new User();
		user.setUsername(username);
		user.setPassword(password);
		user.setDateOfBirth(dateOfBirth);
		user.setSsn(ssn);
		return user;
    }
	
}
