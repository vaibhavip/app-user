package com.gamesys.application.controller;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.gamesys.application.model.User;
import com.gamesys.application.repository.UserRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class AppUserIntegrationTest {
	
	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	UserController userController;

	@Test
	public void whenPostRequestToUsersAndValidUser_thenCorrectResponse() throws Exception {
		String userName = "testuser";
		String ssn = "ssn1";
		String user = String.format("{\"username\" : \"%s\",\r\n" + 
				"	\"password\" : \"22A3v2\",\r\n" + 
				"	\"dateOfBirth\" : \"1910-01-09\",\r\n" + 
				"	\"ssn\" : \"%s\"}", userName, ssn);
		
		mockMvc.perform(
				MockMvcRequestBuilders.post("/add").content(user).contentType(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andReturn();
		
		User persistedUser = userRepository.findByUsernameOrSsn(userName, ssn).get(0);
		
		assertEquals(persistedUser.getUsername(), userName);
		assertEquals(persistedUser.getSsn(), ssn);
	}

	@Test
	public void whenUserExists_thenReturnBadRequest() throws Exception {
		String userName = "Aliko";
		String ssn = "775385075";
		String user = String.format("{\"username\" : \"%s\",\r\n" + 
				"	\"password\" : \"22A3v2\",\r\n" + 
				"	\"dateOfBirth\" : \"1910-01-09\",\r\n" + 
				"	\"ssn\" : \"%s\"}", userName, ssn);
		
		mockMvc.perform(
				MockMvcRequestBuilders.post("/add").content(user).contentType(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isBadRequest())
				.andExpect(MockMvcResultMatchers.content().string("The user already exists"))
				.andReturn();
	}
	
	@Test
	public void whenUserExcluded_thenReturnBadRequest() throws Exception {
		String dateOfBirth = "1815-12-10";
		String ssn = "85385075";
		String user = String.format("{\"username\" : \"testUser\",\r\n" + 
				"	\"password\" : \"22A3v2\",\r\n" + 
				"	\"dateOfBirth\" : \"%s\",\r\n" + 
				"	\"ssn\" : \"%s\"}", dateOfBirth, ssn);
		
		mockMvc.perform(
				MockMvcRequestBuilders.post("/add").content(user).contentType(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isBadRequest())
				.andExpect(MockMvcResultMatchers.content().string("Cannot register user as the user is excluded"))
				.andReturn();
	}
}