# Spring Boot REST API application to validate and register a user

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management
* [JDK](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) - Java™ Platform, Standard Edition Development Kit 
* [Spring Boot](https://spring.io/projects/spring-boot) - Framework to ease the bootstrapping and development of new Spring Applications

## External Tools Used

* [Postman](https://www.getpostman.com/) - API Development Environment (Testing Documentation)

## packages

- `models` — to hold our entities;
- `repositories` — to communicate with the database;
- `services` — to hold our business logic;
- `controllers` — to listen to the client;

- `resources/` - Contains data.sql and property files.
- `resources/application.properties` - It contains application-wide properties. Spring reads the properties defined in this file to configure your application. You can define server’s default port, server’s context path, database URLs etc, in this file.

- `test/` - contains unit and integration tests

- `pom.xml` - contains all the project dependencies

## Installation

To install the app, run through the following steps:

1. Clone the repo and change the directory to be the cloned directory (i.e. app-user)
2. Make sure the machine has Java and Maven installed.
3. From the command line, type `start.sh`, this will start the installation process (it will build the project and start the application)
4. Once application is started, run the following curl command to add user successfully.

```shell
curl -H "Content-Type: application/json" -X POST -d "{\"username\" : \"Test\", \"password\" : \"Pass123\", \"dateOfBirth\" : \"2000-01-01\", \"ssn\" : \"999999999\"}" http://localhost:8081/users/add
```